Feature: User able to assert the mandatory fields


  Scenario: Asserting mandatory fields
    Given am on the home of kalibrate "https://www.kalibrate.com/"
    When i click on Careers (under Company)
    And clicks on Contact us
    And i click on the on the submit button without completing mandatory fields
    Then i should see mandatory field message displayed