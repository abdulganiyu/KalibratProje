package com.base;

import com.page.CareersPage;
import com.page.ContactUsPage;
import com.page.HomePage;
import com.utility.Hooks;
import org.openqa.selenium.WebDriver;

public class BaseClass {

    protected WebDriver driver;
    public CareersPage careersPage;
    public ContactUsPage contactUsPage;
    public HomePage homePage;


    public BaseClass() {
        this.driver = Hooks.driver;
        careersPage = new CareersPage(driver);
        contactUsPage = new ContactUsPage(driver);
        homePage = new HomePage(driver);
    }






}
