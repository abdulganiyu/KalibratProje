package com.utility;



import cucumber.api.java.After;
import cucumber.api.java.Before;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import java.util.concurrent.TimeUnit;

public class Hooks {

    public static WebDriver driver;

    @Before
    public WebDriver setup(){
//C:\Users\Sekinat\IdeaProjects\KalibrateTest\src\test\java\com\browserDriver
        System.setProperty("webdriver.chrome.driver","C:\\Users\\Sekinat\\IdeaProjects\\KalibrateTest\\src\\test\\java\\com\\browserDriver\\chromedriver.exe");

        driver = new ChromeDriver();

        driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);

        return driver;
    }

   // @After
    public void teardown()
    {
        driver.quit();
    }


}
