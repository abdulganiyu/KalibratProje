package com.stepDefn;

import com.base.BaseClass;
import com.utility.Hooks;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.openqa.selenium.WebDriver;

public class KalibrateStepDeftn extends BaseClass {

    WebDriver driver;


    public KalibrateStepDeftn(){
        this.driver = Hooks.driver;

    }


    @Given("^am on the home of kalibrate \"([^\"]*)\"$")
    public void am_on_the_home_of_kalibrate(String arg1) throws Throwable {

        driver.get(arg1);

    }


    @When("^i click on Careers \\(under Company\\)$")
    public void i_click_on_Careers_under_Company() throws Throwable {
        homePage.OpenCareersPage();
    }

    @And("^clicks on Contact us$")
    public void clicks_on_Contact_us() throws Throwable {
        homePage.openContactUsPage();

    }

    @And("^i click on the on the submit button without completing mandatory fields$")
    public void i_click_on_the_on_the_submit_button_without_completing_mandatory_fields() throws Throwable {

        //contactUsPage.clickSubmitButton();
        contactUsPage.enableMandatoryFields();

    }

    @Then("^i should see mandatory field message displayed$")
    public void i_should_see_mandatory_field_message_displayed() throws Throwable {

       contactUsPage.assertMandatoryFields();

    }


}
