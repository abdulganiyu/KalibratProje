package com.page;


import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class HomePage {

    WebDriver driver;
    public HomePage(WebDriver driver){
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }


    //WebElements of the Page headers accross the webpages

    @FindBy(xpath = "//*[@id='hs_menu_wrapper_module_14640756389669909']/ul/li[4]/a") private WebElement Company;
    @FindBy(xpath = "//*[@id='hs_menu_wrapper_module_14640756389669909']/ul/li[4]/ul/li[5]/a") private WebElement Careers;
    //@FindBy(id = "cta_button_489455_29e6ea58-013a-4af9-9a33-6cf9cce487f2") private WebElement ContactUs;
    @FindBy(linkText = "CONTACT US") private WebElement ContactUs;
    @FindBy(linkText = "Accept") private WebElement AcceptPolicy;



    public CareersPage OpenCareersPage(){

        //MOUSE ACTION
        Actions action = new Actions(driver);
        action.moveToElement(Company);
        action.moveToElement(Careers);
        action.click();
        action.perform();
        return new CareersPage(driver);
    }

    public ContactUsPage openContactUsPage(){

        AcceptPolicy.click();

        (ContactUs).click();

        return new ContactUsPage(driver);
    }


}
