package com.page;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class ContactUsPage {

    WebDriver driver;

    public ContactUsPage(WebDriver driver){
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    // InputField WebElement
    @FindBy(name = "firstname") private WebElement FirstName;
    @FindBy(name = "lastname") private WebElement LastName;
    @FindBy(name = "email") private WebElement Email;
    @FindBy(name = "phone") private WebElement PhoneNumber;
    @FindBy(name = "country") private WebElement Country;
    @FindBy(name = "company") private WebElement CompanyName;
    @FindBy(name = "job_role") private WebElement JobRole;
    @FindBy(name = "number_of_sites") private WebElement  NumberofSites;
    @FindBy(name = "LEGAL_CONSENT.processing") private WebElement AgreeCheckBox;

    // Mandatory Message WebElement
    @FindBy(xpath = "//*[@id='hsForm_113a71a3-294d-435c-8c9c-73e62b7be186_2430']/fieldset[1]/div[1]/ul/li/label") private WebElement FirstNameAssert;
    @FindBy(xpath = "//*[@id='hsForm_113a71a3-294d-435c-8c9c-73e62b7be186_2430']/fieldset[1]/div[2]/ul/li/label") private WebElement LastNameAssert;
    @FindBy(xpath = "//*[@id='hsForm_113a71a3-294d-435c-8c9c-73e62b7be186_2430']/fieldset[2]/div[1]/ul/li/label" ) private WebElement EmailAssert;
    @FindBy(xpath = "//*[@id='hsForm_113a71a3-294d-435c-8c9c-73e62b7be186_2430']/fieldset[2]/div[2]/ul/li/label") private WebElement PhoneNumberAssert;
    @FindBy(xpath = "//*[@id='hsForm_113a71a3-294d-435c-8c9c-73e62b7be186_2430']/fieldset[3]/div/ul/li/label") private WebElement CountryAssert;
    @FindBy(xpath = "//*[@id='hsForm_113a71a3-294d-435c-8c9c-73e62b7be186_2430']/fieldset[4]/div/ul/li/label") private WebElement CompanyNameAssert;
    @FindBy(xpath = "//*[@id='hsForm_113a71a3-294d-435c-8c9c-73e62b7be186_2430']/fieldset[5]/div/div/ul/li/label") private WebElement JobRoleAssert;
    @FindBy(xpath = "//*[@id='hsForm_113a71a3-294d-435c-8c9c-73e62b7be186_2430']/fieldset[6]/div/ul/li/label") private WebElement NumberofSitesAssert;
    @FindBy(xpath = "//*[@id='hsForm_113a71a3-294d-435c-8c9c-73e62b7be186_2430']/fieldset[9]/div/div[4]/div/div/ul/li/label") private WebElement AgreeCheckBoxAssert;


    // To Enable the Mandatory Messages
    public void enableMandatoryFields() {
        FirstName.click();
        FirstName.sendKeys("");
        LastName.sendKeys("");
        Email.sendKeys("");
        PhoneNumber.sendKeys("");
        Country.sendKeys("");
        CompanyName.sendKeys("");
        JobRole.sendKeys("");
        NumberofSites.sendKeys("");
        AgreeCheckBox.click();
        AgreeCheckBox.click();
        FirstName.click();

    }

        //To Assert the Mandatory Messages
        public void assertMandatoryFields() {
            FirstNameAssert.isDisplayed();
            assert (FirstNameAssert).getText().equals("Please complete this required field.");
            LastNameAssert.isDisplayed();
            assert (LastNameAssert).getText().equals("Please complete this required field.");
            EmailAssert.isDisplayed();
            assert (EmailAssert).getText().equals("Please complete this required field.");
            PhoneNumberAssert.isDisplayed();
            assert (PhoneNumberAssert).getText().equals("Please complete this required field.");
            CountryAssert.isDisplayed();
            assert(CountryAssert ).getText().equals("Please select an option from the dropdown menu.");
            CompanyNameAssert.isDisplayed();
            assert(CompanyNameAssert).getText().equals("Please complete this required field.");
            JobRoleAssert.isDisplayed();
            assert(JobRoleAssert ).getText().equals("Please select an option from the dropdown menu.");
            NumberofSitesAssert.isDisplayed();
            assert(NumberofSitesAssert).getText().equals("Please select an option from the dropdown menu.");
            AgreeCheckBoxAssert.isDisplayed();
            assert(AgreeCheckBoxAssert).getText().equals("I agree to allow Kalibrate to store and process my personal data.");
            //((JavascriptExecutor)driver).executeScript("scroll(0,400)");
        }




}
