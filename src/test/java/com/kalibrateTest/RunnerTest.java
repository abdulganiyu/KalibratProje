
package com.kalibrateTest;

import cucumber.api.junit.Cucumber;
import cucumber.api.CucumberOptions;
import org.junit.runner.RunWith;

@RunWith(Cucumber.class)
@CucumberOptions(features = "src/test/resources",

              plugin = {"pretty", "html:target/cucumber", "json:target/cucumber.json"},

        monochrome = false
)

public class RunnerTest {

}
